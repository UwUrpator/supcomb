package hsostarter.jvm.http

import lambda._
import scterm._
import cats.effect.{ContextShift, IO}
import io.circe.Decoder
import io.circe.syntax._
import org.http4s.{EntityDecoder, HttpRoutes}
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl

import scala.concurrent.ExecutionContext

object Endpoints extends Http4sDsl[IO] {

  implicit var ioContextShift: ContextShift[IO] =
    IO.contextShift(ExecutionContext.global)

  //object NameMatcher extends QueryParamDecoderMatcher[String]("name")

  implicit def jsonDecoder[A: Decoder]: EntityDecoder[IO, A] = jsonOf[IO, A]

  def endpoints(): HttpRoutes[IO] = HttpRoutes.of[IO] {
    case req @ POST -> Root / "variable" =>
      for {
        variable <- req.as[Variable]
        resp     <- Ok(variable.toString())
      } yield resp

    case req @ POST -> Root / "abstraction" =>
      for {
        abstraction <- req.as[Abstraction]
        resp        <- Ok(abstraction.toString())
      } yield resp

    case req @ POST -> Root / "application" =>
      for {
        application <- req.as[Application]
        resp        <- Ok(application.toString())
      } yield resp

    case req @ POST -> Root / "Term" =>
      for {
        term <- req.as[Term]
        resp <- Ok(term.toString())
      } yield resp

    case req @ POST -> Root / "scvariable" =>
      for {
        scVariable <- req.as[SCVariable]
        resp       <- Ok(scVariable.toString())
      } yield resp

    case req @ POST -> Root / "scapplication" =>
      for {
        scApplication <- req.as[SCApplication]
        resp          <- Ok(scApplication.toString())
      } yield resp

    case req @ POST -> Root / "scdefinition" =>
      for {
        scDefinition <- req.as[SCDefinition]
        resp         <- Ok(scDefinition.toString())
      } yield resp

    case req @ POST -> Root / "SCTerm" =>
      for {
        scTerm <- req.as[SCTerm]
        resp   <- Ok(scTerm.toString())
      } yield resp

    case GET -> Root / "variable" =>
      Ok(Variable("x").asJson)

    case GET -> Root / "abstraction" =>
      Ok(Abstraction(Variable("x"), Variable("y")).asJson)

    case GET -> Root / "application" =>
      Ok(
        Application(Abstraction(Variable("x"), Variable("x")), Variable("y")).asJson,
      )

    case GET -> Root / "scvariable" =>
      Ok(SCVariable("x").asJson)

    case GET -> Root / "scapplication" =>
      Ok(SCApplication(SCVariable("x"), SCVariable("y")).asJson)

    case GET -> Root / "scdefinition" =>
      Ok(
        SCDefinition(List[SCVariable](SCVariable("x")), SCVariable("y")).asJson,
      )

    case GET -> Root / "lift" =>
      Ok(
        SCCore
          .Lift(
            Abstraction(
              Variable("y"),
              Application(
                Variable("x"),
                Variable("y"),
              ),
            ),
          ).asJson,
      )

    case GET -> Root / "apply" =>
      Ok(
        SCCore
          .Apply(
            SCVariable("x"),
            List[SCVariable](SCVariable("a"), SCVariable("b"), SCVariable("c")),
          ).asJson,
      )
  }
}
