package lambda

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto._

sealed trait Term
object Term {
  implicit val termEncoder: Encoder[Term] = deriveEncoder
  implicit val termDecoder: Decoder[Term] = deriveDecoder
}

final case class Variable(name: String) extends Term {
  override def toString(): String = name
}
object Variable {
  implicit val variableEncoder: Encoder[Variable] = deriveEncoder
  implicit val variableDecoder: Decoder[Variable] = deriveDecoder
}

final case class Abstraction(variable: Variable, term: Term) extends Term {
  override def toString: String = s"($variable) -> $term"
}
object Abstraction {
  implicit val abstractionEncoder: Encoder[Abstraction] = deriveEncoder
  implicit val abstractionDecoder: Decoder[Abstraction] = deriveDecoder
}

final case class Application(function: Term, argument: Term) extends Term {
  override def toString: String = s"$function $argument"
}
object Application {
  implicit val applicationEncoder: Encoder[Application] = deriveEncoder
  implicit val applicationDecoder: Decoder[Application] = deriveDecoder
}
