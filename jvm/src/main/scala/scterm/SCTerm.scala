package scterm

import java.util.zip.CRC32

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import lambda.Term

sealed trait SCTerm
object SCTerm {
  implicit val scTermEncoder: Encoder[SCTerm] = deriveEncoder
  implicit val scTermDecoder: Decoder[SCTerm] = deriveDecoder
}

final case class SCVariable(name: String) extends SCTerm {
  override def toString(): String = name
}
object SCVariable {
  implicit val scVarEncoder: Encoder[SCVariable] = deriveEncoder
  implicit val scVarDecoder: Decoder[SCVariable] = deriveDecoder
}

final case class SCDefinition(
  variables: List[SCVariable],
  term: SCTerm,
  name: String,
) extends SCTerm {
  override def toString: String =
    "$ " + name + " = [" + variables.mkString(" ") + "] " + term
}
object SCDefinition {
  implicit val scDefEncoder: Encoder[SCDefinition] = deriveEncoder
  implicit val scDefDecoder: Decoder[SCDefinition] = deriveDecoder

  def apply(vars: List[SCVariable], term: SCTerm): SCDefinition =
    SCDefinition(vars, term, "sc" + term.hashCode().toString)
}

case class SCApplication(function: SCTerm, argument: SCTerm) extends SCTerm {
  override def toString: String = s"($function $argument)"
}
object SCApplication {
  implicit val scAppEncoder: Encoder[SCApplication] = deriveEncoder
  implicit val scAppDecoder: Decoder[SCApplication] = deriveDecoder
}
